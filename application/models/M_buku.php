<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_buku extends CI_Model
{
  	public $table = "tbl_buku";

	public function getBuku(){
		$query = "SELECT *
                    FROM tbl_kategori
                    INNER JOIN tbl_buku USING (id_kategori) ORDER BY id_buku DESC";
        return  $this->db->query($query)->result();
    }

    public function updateBuku($id, $data)
    {
        $this->db->where('id_buku', $id);
        $this->db->update($this->table, $data);
    }


    public function cekId($id)
    {
        $this->db->where('id_buku', $id);
        $data = $this->db->get('tbl_buku')->row();
        return $data;
    }

    function seletctby($id){
        $data = "SELECT *
                    FROM tbl_kategori
                    INNER JOIN tbl_buku USING (id_kategori) WHERE id_buku = '$id'";
        return  $this->db->query($data);
    }



    public function deletebuku($id)
    {
        $this->db->where('id_buku', $id);
        $this->db->delete('tbl_buku');
    }

}