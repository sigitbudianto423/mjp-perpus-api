<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_transaksi extends CI_Model
{

    function dipinjam(){
        $query = "SELECT *
                    FROM tbl_done
                    INNER JOIN tbl_orang USING (id_orang) WHERE status = 'dipinjam'";
        return  $this->db->query($query)->result();
    }

    function dikembali(){
        $query = "SELECT *
                    FROM tbl_done
                    INNER JOIN tbl_orang USING (id_orang) WHERE status = 'dikembalikan'";
        return  $this->db->query($query)->result();
    }

    function telat(){
        $query = "SELECT *
                    FROM tbl_done
                    INNER JOIN tbl_orang USING (id_orang) WHERE status = 'dipinjam'";
        return  $this->db->query($query);
    }

    function simpan_barang($data)
    {
        $this->db->insert('tbl_transaksi',$data);
    }


    public function hapus_item($id_detail)
    {
        //$this->db->where('id_client', $idClient);
        $this->db->where('id_transaksi', $id_detail);
        $this->db->delete('tbl_transaksi');
    }


    function selesai_belanja($data)
    {
        $this->db->insert('tbl_done',$data);
        $last_id = $this->db->query("select id_done from tbl_done order by id_done desc")->row_array();
        $this->db->query("update tbl_transaksi set id_done='".$last_id['id_done']."' where done='0'");
        $this->db->query("update tbl_transaksi set done='1' where done='0'");
    }

    function diskon(){
        
        $dis = $this->db->get_where('tbl_transaksi',['done' => 0]);
        return $dis->num_rows();
    }

    function bayar(){
        $byr =$this->db->query( "select sum(harga)as bayar from tbl_transaksi where done = 0");
        return $byr->row();
    }

    function byr_denda($id){
        $byr =$this->db->query( "select sum(harga)as bayar from tbl_transaksi where id_done = '$id'");
        return $byr->row();
    }


    public function getBuku(){
		$query = "SELECT *
                    FROM tbl_transaksi
                    INNER JOIN tbl_buku USING (id_buku) WHERE done= 0 ";
        return  $this->db->query($query)->result();
    }

    public function cekId($id)
    {
        $this->db->where('id_done', $id);
        $data = $this->db->get('tbl_done');
        return $data;
    }

    public function deletetran($id)
    {
        $this->db->where('id_done', $id);
        $this->db->delete('tbl_done');
    }

    public function deletedetail($id)
    {
        $this->db->where('id_done', $id);
        $this->db->delete('tbl_transaksi');
    }

    public function edittran($id, $data)
    {
        $this->db->where('id_done', $id);
        $this->db->update('tbl_done', $data);
    }
}