<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_Customer extends CI_Model
{
    public $table = "tbl_orang";

    public function getorang($id, $idUser)
    {
        $this->db->where('id_user', $idUser);
        if ($id == null) {
            $orang = $this->db->get($this->table)->result();
        } else {
            $this->db->where('id_orang', $id);
            $orang = $this->db->get($this->table)->row();
        }
        return $orang;
    }

    public function updateorang($id, $data)
    {
        $this->db->where('id_orang', $id);
        $this->db->update($this->table, $data);
    }


    public function cekId($id)
    {
        $this->db->where('id_orang', $id);
        $data = $this->db->get($this->table)->row();
        return $data;
    }
    public function cekEmail($email)
    {
        $this->db->where('email', $email);
        $data = $this->db->get('tbl_user')->row();
        return $data;
    }

    public function deleteorang($id)
    {
        $this->db->where('id_orang', $id);
        $this->db->delete($this->table);
    }

    public function deleteuser($email)
    {
        $this->db->where('email', $email);
        $this->db->delete('tbl_user');
    }

    function seletctby($id)
    {
        $data = $this->db->get_where('tbl_orang', ['id_orang' => $id]);
        return $data;
    }
}
