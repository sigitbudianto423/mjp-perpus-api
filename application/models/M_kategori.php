<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_kategori extends CI_Model
{
    public $table = "tbl_kategori";

    public function getkategori()
    {

        //$this->db->where('id_user', $idUser);
        // if ($id == null) {
        //     $kategori = $this->db->get($this->table)->result();
        // } else {
        //     $this->db->where('id_kategori', $id);
        //     $kategori = $this->db->get($this->table)->row();
        // }
        $kategori = $this->db->get($this->table)->result();
        return $kategori;
    }

    public function updateKategori($id, $data)
    {
        $this->db->where('id_kategori', $id);
        $this->db->update($this->table, $data);
    }


    public function cekId($id)
    {
        $this->db->where('id_kategori', $id);
        $data = $this->db->get('tbl_kategori')->row();
        return $data;
    }

    function seletctby($id)
    {
        $data = $this->db->get_where('tbl_kategori', ['id_kategori' => $id]);
        return $data;
    }



    public function deleteKategori($id)
    {
        $this->db->where('id_kategori', $id);
        $this->db->delete('tbl_kategori');
    }
}
