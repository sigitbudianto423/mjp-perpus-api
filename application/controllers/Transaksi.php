<?php
defined('BASEPATH') or exit('No direct script access allowed');

use chriskacerguis\RestServer\RestController;

class Transaksi extends RestController
{

    function __construct()
    {
        parent::__construct();
        $this->load->library('validation');
        $this->validation->validationToken();
        $this->load->model('m_transaksi');
        $this->load->model('m_buku');
        $this->load->model('m_customer');
    }


    function dipinjam_get()
    {
        $pinjam = $this->m_transaksi->dipinjam();
        if ($pinjam) {
            $this->response([
                'status' => true,
                'message' => ' Data Pinjam Berhasi ditemukan',
                'data' => $pinjam
            ], 200);
        } else {
            $this->response([
                'status' => false,
                'message' => 'Data Pinjam Tidak ditemukan'
            ], 404);
        }
    }

    function dikembalikan_get()
    {
        $pinjam = $this->m_transaksi->dikembali();
        if ($pinjam) {
            $this->response([
                'status' => true,
                'message' => ' Data Pinjam Berhasi ditemukan',
                'data' => $pinjam
            ], 200);
        } else {
            $this->response([
                'status' => false,
                'message' => 'Data Pinjam Tidak ditemukan'
            ], 200);
        }
    }

    public function index_get()
    {

        $buku = $this->m_transaksi->getBuku();
        if ($buku) {
            $this->response([
                'status' => true,
                'message' => 'buku Berhasi ditemukan',
                'data' => $buku
            ], 200);
        } else {
            $this->response([
                'status' => false,
                'message' => 'buku Tidak ditemukan'
            ], 200);
        }
    }

    function simpan_post()
    {
        $id_produk = $this->input->post('id_buku');
        $produk = $this->db->get_where('tbl_buku', array('id_buku' => $id_produk))->row_array();
        $idBuku = $produk['id_buku'];
        $harga = 10000;
        $done = 0;

        // var_dump($id_produk); die;

        $data = [
            'id_buku' => $id_produk,
            'done' => $done,
            'harga' => $harga
        ];

        if ($idBuku == true) {
            $this->m_transaksi->simpan_barang($data);
            $this->response([
                'status' => true,
                'message' => 'berhasil disimpan'
            ], 200);
        } else {
            $this->response([
                'status' => false,
                'message' => 'Gagal disimpan'
            ], 404);
        }
    }


    function hapusitem_delete()
    {
        $id = $this->delete('id_transaksi');
        $id_detail = $this->db->get_where('tbl_transaksi', array('id_transaksi' => $id))->num_rows();
        //var_dump($id_detail); die;
        //$idClient = $this->validation->validationToken()->id_client;

        if ($id_detail == false) {
            $this->response([
                'status' => false,
                'message' => 'Produk Gagal di di Hapus'
            ], 404);
        } else {

            $this->m_transaksi->hapus_item($id);

            $this->response([
                'status' => true,
                'message' => 'Produk Berhasil di Hapus'
            ], 200);
        }
    }



    function selesai_pinjam_post()
    {
        $tanggal =  date('Y-m-d');
        $tgl_kem = $this->input->post('tgl_kembali');
        $user = $this->input->post('id_orang');
        $id_op = $this->db->get_where('tbl_orang', array('id_orang' => $user))->row_array();
        $status = 'dipinjam';
        $sekarang = new DateTime();
        $tglkem = new DateTime($tgl_kem);

        //$total = $this->input->post('total_harga');
        $diskon = $this->m_transaksi->diskon();
        $byr = $this->m_transaksi->bayar();
        // var_dump();
        // die;
        if ($tglkem < $sekarang) {
            $this->response([
                'status' => true,
                'message' => 'Tanggal Tidak boleh kurang dari Tanggal sekarang '
            ], 404);
        }



        if ($diskon == false) {
            $this->response([
                'status' => false,
                'message' => 'Buku Tidak ada, mohon tambahkan buku '
            ], 404);
        }

        if ($id_op == true) {
            if ($diskon >= 5) {

                $di = $byr->bayar * (10 / 100);
                $dis = $byr->bayar - $di;
                $data = array(
                    'id_orang' => $id_op['id_orang'],
                    'tgl_pinjam' => $tanggal,
                    'tgl_kembali' => $tgl_kem,
                    'status' => $status,
                    'total' => $dis

                );
                $this->m_transaksi->selesai_belanja($data);

                $this->response([
                    'status' => true,
                    'message' => 'selamat anda mendapatkan diskon 10%'
                ], 200);
            } else {
                $data = array(
                    'id_orang' => $id_op['id_orang'],
                    'tgl_pinjam' => $tanggal,
                    'tgl_kembali' => $tgl_kem,
                    'status' => $status,
                    'total' => $byr->bayar

                );
                $this->m_transaksi->selesai_belanja($data);
                $this->response([
                    'status' => true,
                    'message' => 'terima kasih telah meminjam'
                ], 200);
            }
        } else {
            $this->response([
                'status' => true,
                'message' => 'nama anda tidak terdaftar'
            ], 404);
        }
    }


    public function book_get($id = null)
    {
        $idUser = $this->validation->validationToken()->id_user;
        $book = $this->m_buku->getBuku($id, $idUser);
        if ($book) {
            $this->response([
                'status' => true,
                'message' => 'Kategori Berhasi ditemukan',
                'data' => $book
            ], 200);
        } else {
            $this->response([
                'status' => false,
                'message' => 'Kategori Tidak ditemukan'
            ], 404);
        }
    }

    public function orang_get($id = null)
    {
        $idUser = $this->validation->validationToken()->id_user;
        $orang = $this->m_customer->getorang($id, $idUser);
        if ($orang) {
            $this->response([
                'status' => true,
                'message' => 'Kategori Berhasi ditemukan',
                'data' => $orang
            ], 200);
        } else {
            $this->response([
                'status' => false,
                'message' => 'Kategori Tidak ditemukan'
            ], 404);
        }
    }


    function h_pinjam_delete()
    {
        $id = $this->delete('id_done');
        // var_dump($this->m_customer->cekId($id)); die;
        if ($this->m_transaksi->cekId($id) == false) {
            $this->response([
                'status' => false,
                'message' => 'Peminjam Gagal di di Hapus'
            ], 404);
        } else {

            $this->m_transaksi->deletedetail($id);
            $this->m_transaksi->deletetran($id);
            $this->response([
                'status' => true,
                'message' => 'Peminjam Berhasil di Hapus'
            ], 200);
        }
    }


    function dikembalikan_put()
    {
        $id = $this->put('id_done');
        if ($this->m_transaksi->cekId($id)->num_rows() == false) {
            $this->response([
                'status' => false,
                'message' => 'Transaksi Gagal di di dikembalikan'
            ], 404);
        } else {

            $idc = $this->m_transaksi->cekId($id)->row();
            $kembali = $idc->tgl_kembali;
            $sekarang = new DateTime();
            $byr = $this->m_transaksi->byr_denda($id);
            $datakem = new DateTime($kembali);
            $status = 'dikembalikan';
            $perbedaan = $sekarang->diff($datakem)->format("%a");
            $d = $byr->bayar * (5 / 100);
            $denda = $d * $perbedaan;

            $data = [
                'status' => $status,
                'denda' => $denda
            ];
            //var_dump($denda); die;
            $this->m_transaksi->edittran($id, $data);
            $this->response([
                'status' => true,
                'message' => 'transaksi Berhasil di kembalikan'
            ], 200);
        }
    }

    function telat_get()
    {
        $pinjam = $this->m_transaksi->telat()->result();
        $telat = $pinjam[0]->tgl_kembali;
        $tlt = new DateTime($telat);
        $sekarang = new DateTime();
        //var_dump($telat < $sekarang); die;


        // var_dump($tlt); die; 
        if ($tlt < $sekarang) {
            $this->response([
                'status' => true,
                'message' => 'Data telat Mengembalikan Buku',
                'data' => $pinjam
            ], 200);
        }

        $this->response([
            'status' => true,
            'message' => ' Data Pinjam tepat waktu',
        ], 404);
    }

    function key_get()
    {
        $diskon = $this->m_transaksi->diskon();
        $byr = $this->m_transaksi->bayar();

        if ($diskon >= 5) {

            $di = $byr->bayar * (10 / 100);
            $dis = $byr->bayar - $di;
            $this->response([
                'status' => true,
                'message' => ' diskon berhasil diambil',
                'data' => $dis,
                'diskon' => $di
            ], 200);
        } else {
            $this->response([
                'status' => true,
                'message' => ' diskon berhasil diambil',
                'data' => $byr->bayar,
                'diskon' => 0
            ], 200);
        }
    }
}
