<?php
defined('BASEPATH') or exit('No direct script access allowed');

use chriskacerguis\RestServer\RestController;

class Buku extends RestController
{

    function __construct()
    {
        parent::__construct();
        $this->load->library('validation');
        $this->validation->validationToken();
        $this->load->model('m_buku');
        $this->load->model('m_kategori');
    }

    public function kat_get($id = null)
    {
        $idUser = $this->validation->validationToken()->id_user;
        $kat = $this->m_kategori->getkategori($id, $idUser);
        if ($kat) {
            $this->response([
                'status' => true,
                'message' => 'Kategori Berhasi ditemukan',
                'data' => $kat
            ], 200);
        } else {
            $this->response([
                'status' => false,
                'message' => 'Kategori Tidak ditemukan'

            ], 404);
        }
    }

    public function index_get()
    {

        //$idUser = $this->validation->validationToken()->id_user;
        $buku = $this->m_buku->getBuku();
        if ($buku) {
            $this->response([
                'status' => true,
                'message' => 'buku Berhasi ditemukan',
                'data' => $buku
            ], 200);
        } else {
            $this->response([
                'status' => false,
                'message' => 'buku Tidak ditemukan'

            ], 200);
        }
    }


    public function index_post()
    {
        $config['upload_path']          = './asset/img/';
        $config['allowed_types']        = 'gif|jpg|png';
        $nama_image = $_FILES['gambar']['name'];
        $nama_buku = $this->input->post('nama_buku');
        $id_kategori = $this->input->post('id_kategori');
        $idUser = $this->validation->validationToken()->id_user;

        $this->form_validation->set_rules('nama_buku', 'Nama Buku', 'required');
        $this->form_validation->set_rules('id_kategori', 'Id Kategori', 'required');
        // $this->form_validation->set_rules('gambar', 'Gambar', 'required');
        if ($this->form_validation->run() == false) {
            $this->response([
                'status' => false,
                'message' => 'Mohon lengkapi Dulu'
            ], 404);
        }



        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('gambar')) {
            $this->response([
                'status' => false,
                'message' => 'Data Gagal di Upload'
            ], 404);
        } else {
            $data = [
                "id_user" => $idUser,
                "nama_buku" => $nama_buku,
                "id_kategori" => $id_kategori,
                "gambar" => $nama_image
            ];
            $this->db->insert('tbl_buku', $data);
            $this->response([
                'status' => true,
                'message' => 'Data berhasil di Upload'
            ], 200);
        }
    }

    public function changeUploadImage($gambarLama)
    {
        $config['upload_path']          = './asset/img/';
        $config['allowed_types']        = 'gif|jpg|png';
        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('gambar')) {
            return $gambarLama;
        } else {
            unlink(FCPATH . '/asset/img/' . $gambarLama);
            return $this->upload->data('file_name');
        }
    }



    function index_put()
    {

        $id = $this->input->post('id_buku');

        //$this->form_validation->set_rules('id_buku', 'buku Nis', 'required');
        $this->form_validation->set_rules('nama_buku', 'Nama Buku ', 'required');
        $this->form_validation->set_rules('id_kategori', 'Nama Buku ', 'required');

        if ($this->m_buku->cekId($id) == false) {
            $this->response([
                'status' => false,
                'message' => 'Id Tidak Ditemukan'
            ], 404);
        }

        $cekId = $this->m_buku->cekId($id);
        $gambarLama = $cekId->gambar;

        $data = [
            'nama_buku' => $this->input->post('nama_buku'),
            'id_kategori' => $this->input->post('id_kategori'),
            'gambar' => $this->changeUploadImage($gambarLama)
        ];

        $hasil =  $this->m_buku->updateBuku($id, $data);
        if (!$hasil == True) {
            $this->response([
                'status' => true,
                'message' => 'buku Berhasil di Edit',
            ], 200);
        } else {
            $this->response([
                'status' => false,
                'message' => 'buku Gagal di Edit'
            ], 404);
        }
    }

    public function index_delete()
    {

        $id = $this->delete('id_buku');
        //var_dump($this->m_buku->cekId($id)); die;
        if ($this->m_buku->cekId($id) == false) {
            $this->response([
                'status' => false,
                'message' => 'buku Gagal di di Hapus'
            ], 404);
        } else {

            $cekId = $this->m_buku->cekId($id);
            $gambarLama = $cekId->gambar;
            unlink(FCPATH . '/asset/img/' . $gambarLama);
            $this->m_buku->deletebuku($id);
            $this->response([
                'status' => true,
                'message' => 'buku Berhasil di Hapus'
            ], 200);
        }
    }

    function selectBy_get()
    {
        $id = $this->get('id_buku');
        $data = $this->m_buku->seletctby($id)->result();
        if ($data != 0) {
            $this->response([
                'status' => true,
                'message' => 'buku Berhasil di diambil',
                'data' => $data
            ], 200);
        } else {
            $this->response([
                'status' => false,
                'message' => 'buku Gagal di diambil'
            ], 404);
        }
    }
}
