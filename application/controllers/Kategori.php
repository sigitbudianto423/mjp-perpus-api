<?php
defined('BASEPATH') or exit('No direct script access allowed');

use chriskacerguis\RestServer\RestController;

class Kategori extends RestController
{

    function __construct()
    {
        parent::__construct();
        $this->load->library('validation');
        $this->validation->validationToken();
        $this->load->model('m_kategori');
    }

    public function index_get($id = null)
    {

        $idUser = $this->validation->validationToken()->id_user;
        $kategori = $this->m_kategori->getkategori($id, $idUser);

        if ($kategori) {
            $this->response([
                'status' => true,
                'message' => 'Kategori Berhasi ditemukan',
                'data' => $kategori
            ], 200);
        } else {
            $this->response([
                'status' => false,
                'message' => 'Kategori Tidak ditemukan'
            ], 404);
        }
    }

    public function index_post()
    {
        $nama_kategori = $this->input->post('nama_kategori');
        $idUser = $this->validation->validationToken()->id_user;

        $this->form_validation->set_rules('nama_kategori', 'Nama Kategori', 'required');
        // $this->form_validation->set_rules('gambar', 'Gambar', 'required');
        if ($this->form_validation->run() == false) {
            $this->response([
                'status' => false,
                'message' => 'Mohon lengkapi Dulu'
            ], 404);
        }

        $data = [
            "id_user" => $idUser,
            "nama_kategori" => $nama_kategori
        ];

        if ($data == TRUE) {
            $this->db->insert('tbl_kategori', $data);
            $this->response([
                'status' => true,
                'message' => 'Kategori Berhasil di Tambah'
            ], 200);
        } else {
            $this->response([
                'status' => false,
                'message' => 'Kategori Gagal di Tambah'
            ], 404);
        }
    }

    function index_put()
    {
        $id = $this->input->post('id_kategori');
        $nama_kategori = $this->input->post('nama_kategori');
        if ($nama_kategori == '') {
            $this->response([
                'status' => false,
                'message' => 'Isi nama kategori'
            ], 404);
        }
        //var_dump($this->m_kategori->cekId($id)); die;
        if ($this->m_kategori->cekId($id) == false) {
            $this->response([
                'status' => false,
                'message' => 'Id Tidak Ditemukan'
            ], 404);
        }

        $data = [
            'nama_kategori' => $nama_kategori
        ];



        $this->m_kategori->updateKategori($id, $data);
        $this->response([
            'status' => true,
            'message' => 'kategori Berhasil di Edit',
        ], 200);
    }


    public function index_delete()
    {

        $id = $this->delete('id_kategori');
        //var_dump($this->m_buku->cekId($id)); die;
        if ($this->m_kategori->cekId($id) == false) {
            $this->response([
                'status' => false,
                'message' => 'kategori Gagal di di Hapus'
            ], 404);
        } else {

            $cekId = $this->m_kategori->cekId($id);
            $this->m_kategori->deleteKategori($id);
            $this->response([
                'status' => true,
                'message' => 'kategori Berhasil di Hapus'
            ], 200);
        }
    }

    function selectBy_get()
    {
        $id = $this->get('id_kategori');
        $data = $this->m_kategori->seletctby($id)->result();
        if ($data != 0) {
            $this->response([
                'status' => true,
                'message' => 'Kategori Berhasil di diambil',
                'data' => $data
            ], 200);
        } else {
            $this->response([
                'status' => false,
                'message' => 'Kategori Gagal di diambil'
            ], 404);
        }
    }
}
