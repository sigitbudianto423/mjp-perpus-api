<?php
defined('BASEPATH') or exit('No direct script access allowed');

use chriskacerguis\RestServer\RestController;

class Customer extends RestController
{

    function __construct()
    {
        parent::__construct();
        $this->load->library('validation');
        $this->validation->validationToken();
        $this->load->model('m_customer');
        // $this->load->model('m_kategori');
    }

    function index_get($id = null)
    {

        $idUser = $this->validation->validationToken()->id_user;
        $kat = $this->m_customer->getorang($id, $idUser);
        if ($kat) {
            $this->response([
                'status' => true,
                'message' => 'Customer Berhasi ditemukan',
                'data' => $kat
            ], 200);
        } else {
            $this->response([
                'status' => false,
                'message' => 'Customer Tidak ditemukan'
            ], 200);
        }
    }


    function index_post()
    {
        $config['upload_path']          = './asset/img/';
        $config['allowed_types']        = 'gif|jpg|png';
        $nama_image = $_FILES['gambar']['name'];
        $no_ktp = $this->input->post('no_ktp');
        $nama_orang = $this->input->post('nama_orang');
        $jeniskelamin = $this->input->post('jenis_kelamin');
        $email = $this->input->post('email');
        $idUser = $this->validation->validationToken()->id_user;
        $validasi = $this->m_customer->cekEmail($email);

        if ($validasi == true) {
            $this->response([
                'status' => false,
                'message' => 'email sudah ada'
            ], 404);
        }

        // $this->form_validation->set_rules('nama_buku', 'Nama Buku', 'required');
        // $this->form_validation->set_rules('id_kategori', 'Id Kategori', 'required');
        // // $this->form_validation->set_rules('gambar', 'Gambar', 'required');
        // if($this->form_validation->run()== false){
        //     $this->response( [
        //         'status' => false,
        //         'message' =>'Mohon lengkapi Dulu'
        //     ], 404 );
        // }

        $lg = [
            'email' => $email,
            'password' => sha1($no_ktp),
            'username' => $nama_orang,
            'level' => 'user'
        ];

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('gambar')) {
            $this->response([
                'status' => false,
                'message' => 'Data Gagal di Upload'
            ], 404);
        } else {
            $data = [
                "id_user" => $idUser,
                "no_ktp" => $no_ktp,
                "nama_orang" => $nama_orang,
                "jenis_kelamin" => $jeniskelamin,
                "email" => $email,
                "gambar" => $nama_image
            ];
            $this->db->insert('tbl_orang', $data);
            $this->db->insert('tbl_user', $lg);
            $this->response([
                'status' => true,
                'message' => 'Data berhasil di Upload'
            ], 200);
        }
    }


    public function changeUploadImage($gambarLama)
    {
        $config['upload_path']          = './asset/img/';
        $config['allowed_types']        = 'gif|jpg|png';
        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('gambar')) {
            return $gambarLama;
        } else {
            unlink(FCPATH . '/asset/img/' . $gambarLama);
            return $this->upload->data('file_name');
        }
    }

    function index_put()
    {

        $id = $this->input->post('id_orang');

        //$this->form_validation->set_rules('id_buku', 'buku Nis', 'required');
        // $this->form_validation->set_rules('nama_buku', 'Nama Buku ', 'required');
        // $this->form_validation->set_rules('id_kategori', 'Nama Buku ', 'required');

        if ($this->m_customer->cekId($id) == false) {
            $this->response([
                'status' => false,
                'message' => 'Id Tidak Ditemukan'
            ], 404);
        }

        $cekId = $this->m_customer->cekId($id);
        $gambarLama = $cekId->gambar;

        $data = [
            'no_ktp' => $this->input->post('no_ktp'),
            'nama_orang' => $this->input->post('nama_orang'),
            'jenis_kelamin' => $this->input->post('jenis_kelamin'),
            'gambar' => $this->changeUploadImage($gambarLama)
        ];

        $hasil =  $this->m_customer->updateorang($id, $data);
        if ($hasil != True) {
            $this->response([
                'status' => true,
                'message' => 'customer Berhasil di Edit',
            ], 200);
        } else {
            $this->response([
                'status' => false,
                'message' => 'customer Gagal di Edit'
            ], 404);
        }
    }

    function index_delete()
    {

        $id = $this->delete('id_orang');
        // var_dump($this->m_customer->cekId($id)); die;
        if ($this->m_customer->cekId($id) == false) {
            $this->response([
                'status' => false,
                'message' => 'orang Gagal di di Hapus'
            ], 404);
        } else {

            $cekId = $this->m_customer->cekId($id);
            $email = $cekId->email;
            $cekEmail = $this->m_customer->cekEmail($email);
            $gambarLama = $cekId->gambar;
            unlink(FCPATH . '/asset/img/' . $gambarLama);
            if ($cekEmail == TRUE) {

                $this->m_customer->deleteorang($id);
                $this->m_customer->deleteuser($email);
                $this->response([
                    'status' => true,
                    'message' => 'orang Berhasil di Hapus'
                ], 200);
            }
        }
    }

    function selectBy_get()
    {
        $id = $this->get('id_orang');
        $data = $this->m_customer->seletctby($id)->result();
        if ($data != 0) {
            $this->response([
                'status' => true,
                'message' => 'Customer Berhasil di diambil',
                'data' => $data
            ], 200);
        } else {
            $this->response([
                'status' => false,
                'message' => 'Customer Gagal di diambil'
            ], 404);
        }
    }
}
